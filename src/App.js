import React from 'react';
import { Switch, Route, BrowserRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button } from '@material-ui/core';

import Navbar from './containers/components/Navbar';
import Home from './containers/home';
import Login from './containers/login';
import Dashboard from './containers/dashboard';
import Events from './containers/events';
import EventMediaList from './containers/eventMediaList';
import Footer from './containers/components/Footer';
import CustomModal from './containers/components/CustomModal';
import * as actions from './store/actions';

function App(props) {
  console.log(props);
  return (
    <BrowserRouter>
      {props && props.user ? props.user.token ? <Navbar /> : null : null}
      <div style={{ minHeight: '82vh', width: '90vw', margin: 'auto' }}>
        <Switch>
          <Route exact path='/' component={Home} />
          {props && props.user && props.user.token ? (
            <>
              <Route exact path='/dashboard' component={Dashboard} />
              <Route exact path='/events' component={Events} />
              <Route exact path='/eventDetails/:eventId' component={EventMediaList} />
            </>
          ) : (
            <Route exact path='/login' component={Login} />
          )}
        </Switch>
      </div>
      {props && props.user ? props.user.token ? <Footer /> : null : null}
      {!(props && props.user && props.user.token) ? <Redirect to='/login' /> : null}
      <CustomModal openModal={props.message} setOpenModal={() => props.setMessage(null)} borderRadius='5px'>
        <div
          style={{
            padding: '1rem 2rem 0',
            maxWidth: '70vmin',
            whiteSpace: 'normal',
            overflow: 'hidden'
          }}
        >
          <div style={{ color: '#4B596C', fontSize: '20px', paddingBottom: '12px' }}>{props.message}</div>
          <div className='text-center py-2'>
            <Button
              variant='contained'
              style={{ color: '#fff', backgroundColor: '#5853DC' }}
              onClick={() => props.setMessage(null)}
            >
              Ok
            </Button>
          </div>
        </div>
      </CustomModal>
    </BrowserRouter>
  );
}

const mapStateToProps = (state) => ({
  user: state.auth.user,
  message: state.auth.message
});

const mapDispatchToProps = (dispatch) => ({
  setMessage: (payload) => dispatch(actions.setMessage(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
