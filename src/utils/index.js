export const getFormattedDate = (date, separator) => {
  const d = new Date(date);
  return `${d.getDate()}${separator}${d.getMonth()}${separator}${d.getFullYear()}`;
};

export const getFormattedTime = (date, separator) => {
  const d = new Date(date);
  const hour = d.getHours();
  return `${hour % 12 === 0 ? '12' : hour % 12}${separator}${d.getMinutes()}${separator}${d.getSeconds()} ${
    hour >= 12 ? 'PM' : 'AM'
  }`;
};
