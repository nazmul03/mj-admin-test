import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { isMobile } from 'react-device-detect';

import * as actions from '../../store/actions';
import MediaList from '../mediaList';
import TimeImage from '../../assets/images/time.png';
import CalendarImage from '../../assets/images/calendar.png';
// import FilterImage from '../../assets/images/Filter.png';
import { getFormattedDate, getFormattedTime } from '../../utils';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  searchBox: {
    width: '100%',
    padding: '10px 20px',
    border: 'none',
    backgroundColor: '#fff',
    borderRadius: '5px',
    color: '#4B596C',
    boxShadow: theme.shadows[5]
  },
  searchOptions: {
    fontSize: '16px',
    color: '#4B596C',
    borderRadius: '10px',
    border: 'none'
  }
}));

const Dashboard = (props) => {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [searchName, setSearchName] = React.useState(null);
  const [refreshTime, setRefreshTime] = React.useState(null);

  React.useEffect(() => {
    props.getAllMediaOfUser({
      creatorId: props && props.user && props.user.id,
      pageNumber: page + 1,
      name: searchName
    });
    props.getEventsOfUser({ creatorId: props && props.user && props.user.id });
    setRefreshTime(new Date());
    // eslint-disable-next-line
  }, [page, searchName]);

  const handleNameChange = (e) => {
    setSearchName(e.target.value);
  };

  console.log(props);

  return (
    <div style={{ overflow: isMobile ? 'auto' : 'none' }}>
      <div className='row py-4'>
        <div
          className='col-12 col-sm-4'
          style={{ textAlign: isMobile ? 'center' : 'left', padding: isMobile ? '6px 0' : '0' }}
        >
          <div style={{ fontSize: '1.5rem', color: '#4B596C', fontWeight: '600' }}>Media Submissions</div>
        </div>
        <div className='col text-center' style={{ margin: isMobile ? '6px 0 6px 6px' : 0 }}>
          <div className='row'>
            <div className='col-12'>
              <select className={classes.searchBox} native value={searchName} onChange={handleNameChange} label='Name'>
                <option className={classes.searchOptions} aria-label='None' value=''>
                  All Events
                </option>
                {props && props.events
                  ? props.events.map((event) => (
                      <option className={classes.searchOptions} value={event.name}>
                        {event.name}
                      </option>
                    ))
                  : null}
              </select>
            </div>
            {/* <div className='col-2'>
              <img src={FilterImage} alt='Filter' width='40' />
            </div> */}
          </div>
        </div>

        <div
          className='col-12 col-sm-4'
          style={{ textAlign: isMobile ? 'center' : 'right', padding: isMobile ? '6px 0' : 0 }}
        >
          <div style={{ fontSize: '12px', color: '#4B596C' }}>
            <span style={{ fontWeight: '700', paddingRight: '15px' }}>Last Refreshed: </span>
            <span style={{ paddingRight: '2px' }}>
              <img src={TimeImage} alt='Time' />
            </span>
            <span style={{ paddingRight: '15px' }}>{getFormattedTime(refreshTime, ':')}</span>
            <span style={{ paddingRight: '2px' }}>
              <img src={CalendarImage} alt='Calendar' />
            </span>
            <span style={{ paddingRight: '2px' }}>{getFormattedDate(refreshTime, '/')}</span>
          </div>
        </div>
      </div>
      <MediaList
        mediaFromEvent={props && props.allMedia}
        numberOfMedia={props && props.numberOfAllMedia}
        page={page}
        setPage={setPage}
        eventDetails
      />
    </div>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  userDetails: state.auth.userDetails,
  allMedia: state.event.allMedia,
  numberOfAllMedia: state.event.numberOfAllMedia,
  events: state.event.events
});

const mapDispatchToProps = (dispatch) => ({
  getAllMediaOfUser: (payload) => dispatch(actions.getAllMediaOfUser(payload)),
  getEventsOfUser: (payload) => dispatch(actions.getEventsOfUser(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Dashboard));
