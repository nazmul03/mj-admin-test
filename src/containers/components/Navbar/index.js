import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';

import * as actions from '../../../store/actions';
import UserLogo from '../../../assets/images/userLogo.png';
import Logout from '../../../assets/images/Logout.png';

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    paddingBottom: '10px'
  },
  appbar: {
    boxShadow: 'none',
    width: '90%',
    margin: 'auto'
  },
  toolbar: {
    padding: '5px'
  },
  options: {
    margin: '0 auto',
    display: 'inherit'
  },
  dashboardLinkArea: {
    borderBottom: (props) => (props.pathname === '/dashboard' ? '4px solid #007bff' : 'none'),
    margin: '0 15px'
  },
  dashboardLink: {
    color: (props) => (props.pathname === '/dashboard' ? '#45435F' : '#4B596C'),
    fontWeight: (props) => (props.pathname === '/dashboard' ? '600' : '400'),
    textTransform: 'none',
    fontSize: (props) => (props.pathname === '/dashboard' ? '16px' : '14px')
  },
  eventLinkArea: {
    borderBottom: (props) => (props.pathname === '/events' ? '4px solid #007bff' : 'none'),
    margin: '0 15px'
  },
  eventLink: {
    color: (props) => (props.pathname === '/events' ? '#45435F' : '#4B596C'),
    fontWeight: (props) => (props.pathname === '/events' ? '600' : '400'),
    textTransform: 'none',
    fontSize: (props) => (props.pathname === '/events' ? '16px' : '14px')
  },
  logoutButton: {
    padding: '10px',
    cursor: 'pointer'
  }
}));

function Navbar(props) {
  const classes = useStyles({
    pathname: props && props.location.pathname
  });

  const logoutClick = () => {
    props.logout({ history: props.history });
  };
  console.log(props);

  return (
    <div className={classes.root}>
      <AppBar position='static' color='transparent' className={classes.appbar}>
        <Toolbar className={classes.toolbar}>
          <Button>
            <img src={UserLogo} alt='Logo' width='120' />
          </Button>

          <div className={classes.options}>
            <div className={classes.dashboardLinkArea}>
              <Link className={classes.dashboardLink} to='/dashboard'>
                Dashboard
              </Link>
            </div>
            <div className={classes.eventLinkArea}>
              <Link className={classes.eventLink} to='/events'>
                Events
              </Link>
            </div>
          </div>

          <Tooltip title='Logout' aria-label='logout'>
            <div className={classes.logoutButton} onClick={logoutClick}>
              <img src={Logout} alt='Logout Button' width='40' />
            </div>
          </Tooltip>
        </Toolbar>
      </AppBar>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => ({
  logout: (history) => dispatch(actions.logout(history))
});

export default connect(null, mapDispatchToProps)(withRouter(Navbar));
