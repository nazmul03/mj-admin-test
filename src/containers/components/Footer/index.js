import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import BottomLogo from '../../../assets/images/bottom-logo.png';

const useStyles = makeStyles(() => ({
  root: {
    textAlign: 'center',
    padding: '10px'
  }
}));

const Footer = (props) => {
  const classes = useStyles();
  console.log(props);

  return (
    <footer className={classes.root}>
      <span style={{ color: '#4B596C', fontSize: '14px', fontWeight: '600' }}>Powered By </span>
      <img src={BottomLogo} alt='BottomLogo' />
    </footer>
  );
};

export default Footer;
