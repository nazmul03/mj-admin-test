import React from 'react';

import Box from '../Box';

const audioType = 'audio/mp3';

class Recorder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: {},
      seconds: 0,
      isPaused: false,
      recording: false,
      medianotFound: false,
      audios: [],
      audioBlob: null
    };
    this.timer = 0;
  }

  handleAudioPause(e) {
    e.preventDefault();
    clearInterval(this.timer);
    this.mediaRecorder.pause();
    this.setState({ pauseRecord: true });
  }
  handleAudioStart(e) {
    e.preventDefault();
    this.startTimer();
    this.mediaRecorder.resume();
    this.setState({ pauseRecord: false });
  }

  startTimer = () => {
    this.timer = setInterval(this.countDown, 1000);
  };

  countDown = () => {
    let seconds = this.state.seconds + 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds
    });
  };

  secondsToTime(secs) {
    let hours = Math.floor(secs / (60 * 60));

    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    let obj = {
      h: hours,
      m: minutes,
      s: seconds
    };
    return obj;
  }

  async componentDidMount() {
    navigator.getUserMedia =
      navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
    if (navigator.mediaDevices) {
      const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
      this.mediaRecorder = new MediaRecorder(stream);
      this.chunks = [];
      this.mediaRecorder.ondataavailable = (e) => {
        if (e.data && e.data.size > 0) {
          this.chunks.push(e.data);
        }
      };
    } else {
      this.setState({ medianotFound: true });
    }
  }

  startRecording(e) {
    e.preventDefault();
    this.chunks = [];
    this.mediaRecorder.start(10);
    this.startTimer();
    this.setState({ recording: true });
  }

  stopRecording(e) {
    clearInterval(this.timer);
    this.setState({ time: {} });
    e.preventDefault();
    this.mediaRecorder.stop();
    this.setState({ recording: false });
    this.saveAudio();
  }

  // handleRest() {
  //   this.setState({
  //     time: {},
  //     seconds: 0,
  //     isPaused: false,
  //     recording: false,
  //     medianotFound: false,
  //     audios: [],
  //     audioBlob: null
  //   });
  //   this.props.handleRest(this.state);
  // }

  saveAudio() {
    const blob = new Blob(this.chunks, { type: audioType });
    const audioURL = window.URL.createObjectURL(blob);
    const audios = [audioURL];
    this.setState({ audios, audioBlob: blob });
    this.props.handleAudioUpload(blob, audioURL);
    this.props.handleAudioStop({
      url: audioURL,
      blob: blob,
      chunks: this.chunks,
      duration: this.state.time
    });
  }

  // uploadRecording() {
  //   this.props.handleAudioUpload(this.state.audioBlob, this.state.audios[0]);
  // }

  render() {
    const { recording, audios, time, medianotFound, pauseRecord } = this.state;
    const { showUIAudio, audioURL } = this.props;

    return (
      <div
        style={{
          borderRadius: '10px',
          height: '322px',
          width: '364px'
        }}
      >
        {!medianotFound ? (
          <div>
            {/* <div>
              <Box says={<ClearAllIcon />} does={() => this.handleRest()} theme={this.props.theme} />
            </div> */}
            <div>
              <div style={{ height: '125px' }}>
                {audioURL !== null && showUIAudio ? (
                  <div
                    style={{
                      padding: '2rem 0',
                      margin: 'auto',
                      width: 'fit-content'
                    }}
                  >
                    <audio
                      controls
                      style={{
                        borderColor: this.props.theme.headerTextColor,
                        borderStyle: 'solid',
                        borderWidth: '2px',
                        borderRadius: '2rem'
                      }}
                    >
                      <source src={audios[0]} type='audio/ogg' />
                      <source src={audios[0]} type='audio/mpeg' />
                    </audio>
                  </div>
                ) : null}
              </div>
              <div
                style={{
                  padding: '1rem 0',
                  fontSize: '2rem',
                  textAlign: 'center',
                  color: this.props.theme.headerTextColor
                }}
              >
                <span>{time.m !== undefined ? `${time.m <= 9 ? '0' + time.m : time.m}` : '00'}</span>
                <span>:</span>
                <span>{time.s !== undefined ? `${time.s <= 9 ? '0' + time.s : time.s}` : '00'}</span>
              </div>
            </div>
            <div style={{ textAlign: 'center' }}>
              {!recording ? (
                <>
                  {/* {this.props.recordComplete ? (
                    <Box says='Done' does={(e) => this.uploadRecording(e)} theme={this.props.theme} />
                  ) : (
                    <Box says='Record' does={(e) => this.startRecording(e)} theme={this.props.theme} />
                  )} */}
                  <Box says='Record' does={(e) => this.startRecording(e)} theme={this.props.theme} />
                </>
              ) : (
                <div>
                  <Box
                    says={pauseRecord ? 'Resume' : 'Pause'}
                    margin='0 2rem 0 0'
                    does={!pauseRecord ? (e) => this.handleAudioPause(e) : (e) => this.handleAudioStart(e)}
                    theme={this.props.theme}
                  />
                  <Box says='Stop' margin='0 2rem 0 0' does={(e) => this.stopRecording(e)} theme={this.props.theme} />
                </div>
              )}
            </div>
          </div>
        ) : (
          <p style={{ color: '#fff', marginTop: 30, fontSize: 25 }}>Seems the site is Non-SSL</p>
        )}
      </div>
    );
  }
}
export default Recorder;
