const CsvDownloader = (props) => {
  const rows = [
    [
      'Media Id',
      'Media Name',
      'Length',
      'Submitted By',
      'Email',
      'Phone',
      'Social Handle',
      'Submitted On',
      'Event ID',
      'Event Name',
      'Event Status'
    ]
  ];

  props &&
    // eslint-disable-next-line
    props.map((media) => {
      rows.push([
        media.id,
        media.media_url,
        media.meta_data ? media.meta_data.duration || 'N/A' : 'N/A',
        media.get_event_collaborator.name,
        media.get_event_collaborator.email,
        media.get_event_collaborator.mobile || 'N/A',
        media.get_event_collaborator.meta_data ? media.get_event_collaborator.meta_data.social_media || 'N/A' : 'N/A',
        new Date(media.created).toString().substring(0, 25),
        media.collab_event,
        media.get_collab_event.name,
        media.get_collab_event.status
      ]);
    });
  console.log(props, rows);

  let csvContent = 'data:text/csv;charset=utf-8,' + rows.map((e) => e.join(',')).join('\n');
  var encodedUri = encodeURI(csvContent);
  var link = document.createElement('a');
  link.setAttribute('href', encodedUri);
  link.setAttribute('download', 'media_submissions.csv');
  document.body.appendChild(link);

  link.click();
};

export default CsvDownloader;
