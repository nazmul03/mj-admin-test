import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import CancelIcon from '@material-ui/icons/Cancel';
import { Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalBody: {
    width: 'fit-content',
    '&:focus, &:focus-visible': {
      outline: 'none'
    }
  },
  closeArea: {
    textAlign: 'right',
    padding: '6px 0'
  },
  closeButton: {
    padding: '0',
    minWidth: 'fit-content',
    '& span': {
      width: 'fit-content'
    }
  },
  paper: (props) => ({
    backgroundColor: props.backgroundColor || theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    borderRadius: props.borderRadius || '0',
    padding: 0,
    height: 'fit-content',
    width: 'fit-content',
    '&:focus, &:focus-visible': {
      outline: 'none'
    }
  })
}));

export default function TransitionsModal(props) {
  const classes = useStyles({
    backgroundColor: props && props.backgroundColor,
    borderRadius: props && props.borderRadius
  });

  return (
    <Modal
      aria-labelledby='transition-modal-title'
      aria-describedby='transition-modal-description'
      className={classes.modal}
      open={props.openModal}
      onClose={() => props.setOpenModal(false)}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
    >
      <Fade in={props.openModal}>
        <div className={classes.modalBody}>
          {props.closeButton ? (
            <div className={classes.closeArea}>
              <Button onClick={() => props.setOpenModal(false)} className={classes.closeButton}>
                <CancelIcon color='error' fontSize='large' />
              </Button>
            </div>
          ) : null}
          <div className={classes.paper}>{props.children}</div>
        </div>
      </Fade>
    </Modal>
  );
}
