import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';

import Progress from '../Progress';

const useStyles = makeStyles({
  container: {
    width: '100px',
    padding: '10px',
    overflow: 'hidden'
  },
  preview: {
    position: 'relative',
    height: '70px',
    width: '70px'
    // '&:hover': {
    //   '& $image': {
    //     opacity: '0.8'
    //   }
    // }
  },
  image: {
    border: '2px solid #ddd',
    boxShadow: '0px 0px 10px 2px #ddd',
    borderRadius: '5px',
    position: 'absolute',
    top: '0',
    left: '0',
    transition: 'all 0.2s',
    opacity: (props) => (props.uploading ? 0.6 : 1)
  },
  button: {
    border: 'none',
    padding: '0'
  },
  closeIcon: {
    color: '#000',
    fontSize: '20px',
    position: 'absolute',
    top: '-10%',
    left: '80%',
    backgroundColor: '#fff',
    borderRadius: '50%',
    visibility: 'visible'
  },
  name: {
    marginTop: '5px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    cursor: 'default'
  }
});

const SingleFile = (props) => {
  const classes = useStyles({ uploading: props && props.uploading && props.uploading.includes(props.name) });

  return (
    <div style={{ display: 'inline-block', padding: '1rem' }}>
      {props.src ? (
        <div className={classes.container}>
          <div className={classes.preview}>
            <img src={props.src} className={classes.image} width='100%' height='100%' alt='imagePreview' />
            {!(props.uploading && props.uploading.includes(props.name)) ? (
              <button onClick={props.removeFromSelectedFiles} className={classes.button}>
                <CloseIcon className={classes.closeIcon} />
              </button>
            ) : null}
          </div>
          {props && props.uploading && props.uploading.includes(props.name) ? (
            <div>
              <Progress />
            </div>
          ) : null}
          <div className={classes.name} data-toggle='tooltip' data-placement='top' title={props.name}>
            {props.name}
          </div>
        </div>
      ) : (
        <span>File format not supported</span>
      )}
    </div>
  );
};

export default SingleFile;
