import React from 'react';

const DragUploadComponent = (props) => {
  const dragOver = (e) => {
    e.preventDefault();
  };

  const dragEnter = (e) => {
    e.preventDefault();
  };

  const dragLeave = (e) => {
    e.preventDefault();
  };

  const fileDrop = (e) => {
    e.preventDefault();
    const files = e.dataTransfer.files;
    if (files.length) {
      props.selectFile(files);
    }
  };

  // const handleFiles = (files) => {
  //   for (let i = 0; i < files.length; i++) {
  //     files[i].selectedFile = window.URL.createObjectURL(files[i]);
  //     if (validateFile(files[i])) {
  //       props.setFileObj((prevArray) => [...prevArray, files[i]]);
  //     } else {
  //       files[i]['invalid'] = true;
  //       setErrorMessage('File type not permitted');
  //     }
  //   }
  // };

  // const validateFile = (file) => {
  //   const validTypes = [
  //     'image/jpeg',
  //     'image/jpg',
  //     'image/png',
  //     'image/gif',
  //     'image/x-icon',
  //     'video/webm',
  //     'video/mp4',
  //     'video/mov',
  //     'video/mkv',
  //     'video/wmv',
  //     'video/flv',
  //     'video/avi',
  //     'video/avchd',
  //     'audio/wav',
  //     'audio/pcm',
  //     'audio/aiff',
  //     'audio/mp3',
  //     'audio/mpeg',
  //     'audio/acc',
  //     'audio/ogg',
  //     'audio/wma'
  //   ];
  //   if (validTypes.indexOf(file.type) === -1) {
  //     return false;
  //   }
  //   return true;
  // };

  return (
    <>
      <div onDragOver={dragOver} onDragEnter={dragEnter} onDragLeave={dragLeave} onDrop={fileDrop}>
        {props.children}
      </div>
    </>
  );
};

export default DragUploadComponent;
