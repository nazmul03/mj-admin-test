import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import * as actions from '../../store/actions';

const Events = (props) => {
  console.log(props);

  React.useState(() => {
    props.getEventsOfUser({ creatorId: props && props.user && props.user.id });
  }, [props]);

  return (
    <div>
      <h1>{`Hello ${props && props.user && props.user.name}`}</h1>
      {props.events &&
        props.events.map((event) => {
          return (
            <div>
              <Link
                to={`/eventDetails/${event.id}`}
              >{`Event ID: ${event.id} Event Name: ${event.name} Status: ${event.status}`}</Link>
            </div>
          );
        })}
    </div>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  userDetails: state.auth.userDetails,
  events: state.event.events
});

const mapDispatchToProps = (dispatch) => ({
  getEventsOfUser: (payload) => dispatch(actions.getEventsOfUser(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Events));
