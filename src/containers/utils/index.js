// export const generateNameFromUrl = (text, url) => {
//   let slashIndexs = [];
//   for (let i = 0; i < url.length; i++) {
//     if (url[i] === '/') {
//       slashIndexs.push(i);
//     }
//   }
//   return `${text}-${url.substring(slashIndexs[slashIndexs.length - 1] + 1, url.length)}`;
// };

export const generateNameFromUrl = (text, extension) => {
  var d = new Date();
  return `${text}-${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}-${d.getDate()}:${d.getMonth()}:${d.getFullYear()}.${extension}`;
};

export const isFbOrInstaAndroidBrowser = () => {
  let ua = navigator.userAgent;
  // return ua.indexOf('FBAN') > -1 || ua.indexOf('FBAV') > -1; // MiuiBrowser
  let fbBrowser = ua.includes('FBAv') || ua.includes('FBAN') || ua.includes('FB_IAB') || ua.includes('Instagram');
  let androidDevice = ua.includes('Android');
  return fbBrowser && androidDevice;
};
