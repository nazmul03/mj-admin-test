import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { isMobile } from 'react-device-detect';

import SingleMedia from '../singleMedia';
import CsvDownloader from '../components/CsvDownloader';

const useStyle = makeStyles((theme) => {
  console.log(theme);
  return {
    root: {
      borderRadius: '16px',
      boxShadow: theme.shadows[10],
      '& > *': {
        borderBottom: 'unset'
      }
    },
    tableHead: {
      backgroundColor: '#E4E4E4'
    },
    tableCell: {
      fontSize: '12px'
    },
    downloadAllButton: {
      cursor: 'pointer',
      backgroundColor: '#fff',
      padding: '2px 10px',
      border: '2px solid #4B596C',
      borderRadius: isMobile ? '1rem' : '2rem',
      boxShadow: 'none',
      transition: 'all 0.4s linear',
      fontSize: '10px'
    }
  };
});

const MediaList = (props) => {
  const classes = useStyle();

  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    props.setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    props.setPage(0);
  };

  return (
    <div className='row m-0'>
      <div className='' style={{ margin: 'auto' }}>
        <TableContainer component={Paper} className={classes.root}>
          <Table aria-label='collapsible table'>
            <TableHead className={classes.tableHead}>
              <TableRow>
                <TableCell className={classes.tableCell} align='center'>
                  SN
                </TableCell>
                <TableCell className={classes.tableCell} align='center'>
                  Media Id
                </TableCell>
                <TableCell className={classes.tableCell} align='center'>
                  Media File Name
                </TableCell>
                <TableCell className={classes.tableCell} align='center'>
                  Media Length
                </TableCell>
                <TableCell className={classes.tableCell} align='center'>
                  Submitted By
                </TableCell>
                <TableCell className={classes.tableCell} align='center'>
                  Email Id
                </TableCell>
                <TableCell className={classes.tableCell} align='center'>
                  Phone No
                </TableCell>
                <TableCell className={classes.tableCell} align='center'>
                  Social Handle
                </TableCell>
                <TableCell className={classes.tableCell} align='center'>
                  Submitted On
                </TableCell>
                {props.eventDetails ? (
                  <TableCell className={classes.tableCell} align='center'>
                    Event ID
                  </TableCell>
                ) : null}
                {props.eventDetails ? (
                  <TableCell className={classes.tableCell} align='center'>
                    Event Name
                  </TableCell>
                ) : null}
                {props.eventDetails ? (
                  <TableCell className={classes.tableCell} align='center'>
                    Event Status
                  </TableCell>
                ) : null}
                <TableCell className={classes.tableCell} align='center'>
                  <div
                    className={classes.downloadAllButton}
                    onClick={() => CsvDownloader(props && props.mediaFromEvent)}
                  >
                    Download As CSV
                  </div>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {props && props.mediaFromEvent
                ? props.mediaFromEvent.map((media, count) => (
                    <SingleMedia
                      key={media.id}
                      media={media}
                      eventDetails={props.eventDetails}
                      count={count}
                      page={props.page}
                    />
                  ))
                : null}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10]}
          component='div'
          count={props.numberOfMedia}
          rowsPerPage={rowsPerPage}
          page={props.page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </div>
    </div>
  );
};

export default MediaList;
