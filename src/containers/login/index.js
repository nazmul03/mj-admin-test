import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PersonIcon from '@material-ui/icons/Person';
import LockIcon from '@material-ui/icons/Lock';
import { makeStyles } from '@material-ui/core/styles';

import logo from '../../assets/images/logo.png';
import * as actions from '../../store/actions';

const useStyles = makeStyles((theme) => {
  // console.log(theme);
  return {
    inputArea: { margin: '5px 0', border: '2px solid #A2ACC4', borderRadius: '5px' },
    iconArea: { borderRight: '2px solid #A2ACC4', padding: '8px' },
    icon: { fontSize: '1.5rem', color: '#A2ACC4' },
    inputBox: { border: 'none', padding: '4px', fontSize: '1rem', '&:hover, &:focus': { outline: 'none' } },
    submit: {
      border: 'none',
      boxShadow: theme.shadows[5],
      padding: '5px 15px',
      backgroundColor: '#2C85E0',
      color: '#FFF',
      borderRadius: '5px',
      '&:hover, &:focus, &:active': {
        outline: 'none'
      }
    }
  };
});

function Login(props) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    username: '',
    password: ''
  });

  const handleChange = (e) => {
    setState({
      ...state,
      [e.target.id]: e.target.value
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    props.loginUser({ user: state, history: props.history });
  };

  console.log(props);

  return (
    <div>
      <div className='row' style={{ marginTop: '20vh' }}>
        <div className='col-10 col-sm-4' style={{ width: 'fit-content', margin: '0 auto' }}>
          <div style={{ textAlign: 'center', padding: '1rem 0' }}>
            <img src={logo} alt='MilJul_Logo' width='80' />
          </div>
          <form onSubmit={handleSubmit}>
            <div className={classes.inputArea}>
              <span className={classes.iconArea}>
                <PersonIcon className={classes.icon} />
              </span>
              <input
                type='text'
                name='username'
                id='username'
                value={state.username}
                placeholder='Username'
                onChange={handleChange}
                className={classes.inputBox}
              />
            </div>
            <div className={classes.inputArea}>
              <span className={classes.iconArea}>
                <LockIcon className={classes.icon} />
              </span>
              <input
                type='password'
                name='password'
                id='password'
                value={state.password}
                placeholder='Password'
                onChange={handleChange}
                className={classes.inputBox}
              />
            </div>
            <div style={{ textAlign: 'center', padding: '5px 0' }}>
              <button type='submit' className={classes.submit}>
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  user: state.auth.user
});

const mapDispatchToProps = (dispatch) => ({
  loginUser: (payload) => dispatch(actions.loginUser(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));
