import React from 'react';
import ReactPlayer from 'react-player';
import { makeStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';

import CustomModal from '../components/CustomModal';
import DownloadIcon from '../../assets/images/download.png';
import { getFormattedDate, getFormattedTime } from '../../utils';

const useRowStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: (props) => (props.count % 2 === 0 ? '#fff' : '#FCFCFC'),
    '& > *': {
      borderBottom: 'unset'
    }
  },
  tableCell: {
    fontSize: '10px'
  },
  mediaName: {
    cursor: 'pointer',
    textDecoration: 'underline',
    textDecorationStyle: 'dashed',
    textDecorationColor: '#4B596C'
  },
  downloadIcon: {
    padding: '2px',
    boxShadow: 'none',
    transition: 'all 0.4s linear'
  },
  reactPlayer: {
    borderRadius: '20px',
    '& video': {
      borderRadius: '20px'
    }
  },
  audioPlayer: {
    borderRadius: '20px'
  },
  image: {
    height: '60vmin',
    borderRadius: '20px'
  }
}));

const SingleMedia = (props) => {
  const classes = useRowStyles({ count: props && props.count });
  const { media } = props;
  const [openModal, setOpenModal] = React.useState(false);
  const [fileType, setFilType] = React.useState(null);

  React.useEffect(() => {
    media && media.media_format ? setFilType(media.media_format) : setFilType(findFileFormat(media.media_url));
  }, [media]);

  const findFileFormat = (filename) => {
    let dotIndexes = [];
    if (filename === null) return '';
    for (let i = 0; i < filename.length; i++) {
      if (filename[i] === '.') {
        dotIndexes.push(i);
      }
    }
    return filename.substring(dotIndexes[dotIndexes.length - 1] + 1, filename.length);
  };

  const getNameFromUrl = (url) => {
    if (url !== '' || url !== null) {
      url = url.substring(68, url.length);
      url = url.replace(/%3A/g, ':');
    } else url = '';
    // return url ? url.substring(40, url.length) : '';
    // return url ? url.substring(url.length - 8, url.length) : '';
    return url;
  };

  const truncatedName = (fullname) => {
    return '...' + fullname.substring(fullname.length - 8, fullname.length);
  };

  return (
    <>
      <React.Fragment>
        <TableRow className={classes.root}>
          <TableCell className={classes.tableCell} align='center'>
            {props.page * 10 + props.count + 1}
          </TableCell>
          <TableCell className={classes.tableCell} align='center'>
            {media.id}
          </TableCell>
          <TableCell className={classes.tableCell} component='th' scope='row'>
            <Tooltip title={getNameFromUrl(media.media_url)}>
              <div onClick={() => setOpenModal(true)} className={classes.mediaName}>
                {truncatedName(getNameFromUrl(media.media_url))}
              </div>
            </Tooltip>
          </TableCell>
          <TableCell className={classes.tableCell} align='center'>
            {media.meta_data ? `${Math.round(media.meta_data.duration)}s` || 'N/A' : 'N/A'}
          </TableCell>
          <TableCell className={classes.tableCell} component='th' scope='row'>
            {media.get_event_collaborator.name}
          </TableCell>
          <TableCell className={classes.tableCell} align='center'>
            {media.get_event_collaborator.email}
          </TableCell>
          <TableCell className={classes.tableCell} align='center'>
            {media.get_event_collaborator.mobile || 'N/A'}
          </TableCell>
          <TableCell className={classes.tableCell} align='center'>
            {media.get_event_collaborator.meta_data
              ? media.get_event_collaborator.meta_data.social_media || 'N/A'
              : 'N/A'}
          </TableCell>
          <TableCell className={classes.tableCell}>
            {getFormattedDate(media.created, '.') + ' ' + getFormattedTime(media.created, ':')}
          </TableCell>
          {props.eventDetails ? (
            <TableCell className={classes.tableCell} align='center'>
              {media.collab_event}
            </TableCell>
          ) : null}
          {props.eventDetails ? (
            <TableCell className={classes.tableCell} align='center'>
              {media.get_collab_event.name}
            </TableCell>
          ) : null}
          {props.eventDetails ? (
            <TableCell className={classes.tableCell} align='center'>
              {media.get_collab_event.status === 'COMPLETED' ? (
                <div>
                  <span style={{ padding: '0 4px' }}>Completed</span>
                  <span style={{ backgroundColor: '#398E15', padding: '2px 8px', borderRadius: '50%' }}> </span>
                </div>
              ) : media.get_collab_event.status === 'IN_PROGRESS' ? (
                <div>
                  <span style={{ padding: '0 4px' }}>In Progress</span>
                  <span style={{ backgroundColor: '#FAE202', padding: '2px 8px', borderRadius: '50%' }}> </span>
                </div>
              ) : (
                media.get_collab_event.status
              )}
            </TableCell>
          ) : null}
          <TableCell className={classes.tableCell} align='center'>
            <Tooltip title='Download this file'>
              <a href={media.media_url}>
                <img src={DownloadIcon} alt='Download Icon' width='30' className={classes.downloadIcon} />
              </a>
            </Tooltip>
          </TableCell>
        </TableRow>
      </React.Fragment>

      <CustomModal
        openModal={openModal}
        setOpenModal={setOpenModal}
        closeButton
        backgroundColor='transparent'
        borderRadius='20px'
      >
        {(fileType && fileType === 'MP4') ||
        (fileType && fileType === 'mp4') ||
        (fileType && fileType === 'webm') ||
        (fileType && fileType === 'MOV') ? (
          <ReactPlayer url={media.media_url} className={classes.reactPlayer} controls width='auto' height='60vmin' />
        ) : (fileType && fileType === 'jpg') ||
          (fileType && fileType === 'png') ||
          (fileType && fileType === 'jpeg') ? (
          <img src={media.media_url} alt='File' className={classes.image} />
        ) : fileType && fileType === 'svg' ? (
          <svg>{media.media_url}</svg>
        ) : fileType && fileType === 'mp3' ? (
          <audio controls className={classes.audioPlayer}>
            <source src={media.media_url} />
            Your browser does not support the audio element.
          </audio>
        ) : fileType && fileType === 'mov' ? (
          <video controls className={classes.reactPlayer} style={{ height: '60vmin' }}>
            <source src={media.media_url} />
            Your browser does not support the video element.
          </video>
        ) : (
          <div>"File Format Not Supported"</div>
        )}
      </CustomModal>
    </>
  );
};

export default SingleMedia;
