import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

function Home(props) {
  console.log(props);
  return (
    <div>
      <h1>Home Page</h1>
    </div>
  );
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    //
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    //
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));
