import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router';
import queryString from 'query-string';

import Header from '../Header';
import Content from '../Content';
import CustomModal from '../components/CustomModal';
import Box from '../components/Box';
import { isFbOrInstaAndroidBrowser } from '../utils';

const Event = (props) => {
  const [data, setData] = React.useState(null);
  const [message, setMessage] = React.useState(null);
  const [logo, setLogo] = React.useState(null);

  const [colabEventId, setColabEventId] = React.useState(null);
  const [collaboratorId, setCollaboratorId] = React.useState(null);
  const [showMediaHandle, setShowMediaHandle] = React.useState(true.toString());

  const REDIRECT_URL = `https://lifelong.miljulapp.com/#/event/${colabEventId}`;

  React.useEffect(() => {
    setColabEventId(props && props.match.params.eventId);
    setCollaboratorId(props && props.match.params.collaboratorId);
    setShowMediaHandle((props && queryString.parse(props.location.search).showMediaHandle) || true.toString());

    if (isFbOrInstaAndroidBrowser()) {
      window.location.assign(`https://api.miljulapp.com/api/v1/common/ifAppRedirect?url=${REDIRECT_URL}`);
    } else {
      colabEventId &&
        axios
          .get(`https://api.miljulapp.com/api/v1/api/v1/collab/collabevent/${colabEventId}`)
          .then((response) => {
            console.log(response);
            if (response.status === 200) {
              let res = response.data.response.response;
              setData(res);
              setLogo(res.meta_data ? (res.meta_data.logo_url ? res.meta_data.logo_url : null) : null);
            } else {
              setMessage('There was some error');
            }
          })
          .catch((error) => {
            console.log(error);
            setMessage('There was some error');
          });
    }
  }, [props, colabEventId, REDIRECT_URL]);

  return (
    <>
      {data && <Header theme={props.theme} logo={logo} />}
      <Content
        data={data && data}
        message={message}
        setMessage={setMessage}
        colabEventId={colabEventId && colabEventId}
        collaboratorId={collaboratorId && collaboratorId}
        showMediaHandle={showMediaHandle}
        theme={props.theme}
      />
      <CustomModal open={message} setOpen={() => setMessage(null)} theme={props.theme}>
        <div style={{ textAlign: 'center' }}>
          <div style={{ fontSize: '2rem', padding: '0.5rem 0 2rem' }}>{message}</div>
          <Box says='Ok' does={() => setMessage(null)} theme={props.theme} />
        </div>
      </CustomModal>
    </>
  );
};

export default withRouter(Event);
