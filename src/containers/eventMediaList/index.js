import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from '../../store/actions';
import MediaList from '../mediaList';

function EventMediaList(props) {
  const [page, setPage] = React.useState(0);

  React.useEffect(() => {
    console.log(page);
    props.getMediaFromEvent({ eventId: props && props.match.params.eventId, pageNumber: page + 1 });
    // eslint-disable-next-line
  }, [page]);

  console.log(props);

  return (
    <>
      <div className='row' style={{ padding: '0', margin: '2rem 1rem' }}>
        <div className='col-2'>
          <span style={{ color: '#737373' }}>Event </span>
          <span>{props && props.mediaFromEvent && props.mediaFromEvent[0].collab_event}</span>
        </div>
        <div className='col-8 text-center'>
          <span style={{ fontSize: '3rem', fontWeight: '600' }}>
            {props && props.mediaFromEvent && props.mediaFromEvent[0].get_collab_event.name}
          </span>
        </div>
        <div className='col-2'>
          <span style={{ color: '#737373' }}>Status </span>
          <span>{props && props.mediaFromEvent && props.mediaFromEvent[0].get_collab_event.status}</span>
        </div>
      </div>
      <MediaList
        mediaFromEvent={props && props.mediaFromEvent}
        numberOfMedia={props && props.numberOfMedia}
        page={page}
        setPage={setPage}
      />
    </>
  );
}

const mapStateToProps = (state) => ({
  user: state.auth.user,
  mediaFromEvent: state.event.mediaFromEvent,
  numberOfMedia: state.event.numberOfMedia
});

const mapDispatchToProps = (dispatch) => ({
  getMediaFromEvent: (payload) => dispatch(actions.getMediaFromEvent(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EventMediaList));
