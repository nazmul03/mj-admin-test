import axios from 'axios';

export default {
  loginUser: (payload) => {
    return axios.post(`https://api.miljulapp.com/api/v1/common/login`, payload);
  },

  getUserDetails: (payload) => {
    return axios.get(`https://api.miljulapp.com/api/v1/api/v1/users/creator?email=${payload}`);
  },

  getEventsOfUser: (payload) => {
    return axios.get(
      `https://api.miljulapp.com/api/v1/api/v1/collab/collabevent?creator=${payload.creatorId}&page_size=50`
    );
  },

  getMediaFromEvent: (payload) => {
    return axios.get(
      `https://api.miljulapp.com/api/v1/api/v1/collab/collabmedia?collab_event=${payload.eventId}&page=${payload.pageNumber}`
    );
  },

  getAllMediaOfCreator: (payload) => {
    console.log('api', payload);
    if (payload.name) {
      return axios.get(
        `https://api.miljulapp.com/api/v1/api/v1/collab/collabmedia?collab_event__creator=${payload.creatorId}&collab_event__name=${payload.name}&page=${payload.pageNumber}`
      );
    } else
      return axios.get(
        `https://api.miljulapp.com/api/v1/api/v1/collab/collabmedia?collab_event__creator=${payload.creatorId}&page=${payload.pageNumber}`
      );
  }
};
