import * as types from '../constants';

const initialState = {
  events: null,
  allMedia: null,
  numberOfAllMedia: null,
  mediaFromEvent: null,
  numberOfMedia: null
};

const saveAllMedia = (state, payload) => ({
  ...state,
  allMedia: payload
});

const saveNumberOfAllMedia = (state, payload) => ({
  ...state,
  numberOfAllMedia: payload
});

const saveEvents = (state, payload) => ({
  ...state,
  events: payload
});

const saveMediaFromEvent = (state, payload) => ({
  ...state,
  mediaFromEvent: payload
});

const saveNumberOfMedia = (state, payload) => ({
  ...state,
  numberOfMedia: payload
});

const clearStorage = (state, payload) => ({});

export default (state = initialState, action) => {
  switch (action.type) {
    case types.SET_ALL_MEDIA_OF_USER:
      return saveAllMedia(state, action.payload);
    case types.SET_NUMBER_OF_ALL_MEDIA:
      return saveNumberOfAllMedia(state, action.payload);
    case types.SET_MEDIA_FROM_EVENT:
      return saveMediaFromEvent(state, action.payload);
    case types.SET_EVENTS_OF_USER:
      return saveEvents(state, action.payload);
    case types.SET_NUMBER_OF_MEDIA:
      return saveNumberOfMedia(state, action.payload);
    case types.CLEAR_STORAGE:
      return clearStorage(state, action.payload);
    default:
      return state;
  }
};
