import * as types from '../constants';

const initialState = {
  user: null,
  userDetails: null,
  message: null
};

const saveUserBasicInfo = (state, payload) => ({
  ...state,
  user: payload
});

const saveUserDetails = (state, payload) => ({
  ...state,
  userDetails: payload
});

const saveMessage = (state, payload) => ({
  ...state,
  message: payload
});

const clearStorage = (state, payload) => ({});

export default (state = initialState, action) => {
  switch (action.type) {
    case types.SAVE_USER_BASIC_INFO:
      return saveUserBasicInfo(state, action.payload);
    case types.SAVE_USER_DETAILS:
      return saveUserDetails(state, action.payload);
    case types.SET_MESSAGE_SAGA:
      return saveMessage(state, action.payload);
    case types.CLEAR_STORAGE:
      return clearStorage(state, action.payload);
    default:
      return state;
  }
};
