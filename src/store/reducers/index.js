import { combineReducers } from 'redux';

import auth from './auth';
import event from './event';

const reducers = combineReducers({
  auth: auth,
  event: event
});

export default reducers;
