import { call, put, takeEvery } from 'redux-saga/effects';

import api from '../../api';

// Login User
function* login_user_saga(request) {
  try {
    const response = yield call(api.loginUser, request.payload.user);
    console.log(response);
    if (response.status === 200) {
      yield put({ type: 'SAVE_USER_BASIC_INFO', payload: response.data });
      request.payload.history.push('/dashboard');
    } else {
      yield put({ type: 'SET_MESSAGE_SAGA', payload: 'Something went wrong with login' });
    }
  } catch (error) {
    console.log(error);
    yield put({ type: 'SET_MESSAGE_SAGA', payload: 'Login credentials are not correct' });
  }
}

// Get user details and use the id to get all the media of the user
function* get_all_media_of_user_saga(request) {
  console.log(request);
  try {
    const mediaResponse = yield call(api.getAllMediaOfCreator, request.payload);
    console.log(mediaResponse);
    if (mediaResponse.status === 200) {
      yield put({ type: 'SET_ALL_MEDIA_OF_USER', payload: mediaResponse.data.response.response.results });
      yield put({ type: 'SET_NUMBER_OF_ALL_MEDIA', payload: mediaResponse.data.response.response.count });
    } else {
      yield put({ type: 'SET_MESSAGE_SAGA', payload: 'Problem in getting all the media of the user' });
    }
  } catch (error) {
    console.log(error);
    yield put({ type: 'SET_MESSAGE_SAGA', payload: 'Problem in getting all the media of the user' });
  }
}

// Get user details and use the id to get all the events of the user
function* get_events_of_user_saga(request) {
  try {
    const eventsResponse = yield call(api.getEventsOfUser, request.payload);
    console.log(eventsResponse);
    if (eventsResponse.status === 200) {
      yield put({ type: 'SET_EVENTS_OF_USER', payload: eventsResponse.data.response.response.results });
    } else {
      yield put({ type: 'SET_MESSAGE_SAGA', payload: 'Problem in getting all the events' });
    }
  } catch (error) {
    console.log(error);
    yield put({ type: 'SET_MESSAGE_SAGA', payload: 'Problem in getting all the events' });
  }
}

// Get all the medias related to that event
function* get_media_from_event_saga(request) {
  try {
    const response = yield call(api.getMediaFromEvent, request.payload);
    console.log(response);
    if (response.status === 200) {
      yield put({ type: 'SET_MEDIA_FROM_EVENT', payload: response.data.response.response.results });
      yield put({ type: 'SET_NUMBER_OF_MEDIA', payload: response.data.response.response.count });
    } else {
      yield put({ type: 'SET_MESSAGE_SAGA', payload: 'Problem in getting all media of this event' });
    }
  } catch (error) {
    console.log(error);
  }
}

function* set_message_action_saga(request) {
  yield put({ type: 'SET_MESSAGE_SAGA', payload: request.payload });
}

// Logout user and set store to null
function* logout_saga(request) {
  try {
    window.localStorage.clear();
    yield put({ type: 'CLEAR_STORAGE', payload: null });
    request.payload.history.push('/login');
  } catch (error) {
    yield put({ type: 'SET_MESSAGE_SAGA', payload: 'Logout Failed' });
  }
}

export default function* watchAll() {
  return (
    yield takeEvery('LOGIN_USER', login_user_saga),
    yield takeEvery('GET_ALL_MEDIA_OF_USER', get_all_media_of_user_saga),
    yield takeEvery('GET_EVENTS_OF_USER', get_events_of_user_saga),
    yield takeEvery('GET_MEDIA_FROM_EVENT', get_media_from_event_saga),
    yield takeEvery('SET_MESSAGE_ACTION', set_message_action_saga),
    yield takeEvery('LOGOUT', logout_saga)
  );
}
