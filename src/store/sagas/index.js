import { all } from 'redux-saga/effects';

import watchAll from './all';

export default function* sagas() {
  yield all([watchAll()]);
}
