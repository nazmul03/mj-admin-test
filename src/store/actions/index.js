import * as types from '../constants';

export const loginUser = (payload) => ({
  type: types.LOGIN_USER,
  payload
});

export const saveUserBasicInfo = (payload) => ({
  type: types.SAVE_USER_BASIC_INFO,
  payload
});

export const getAllMediaOfUser = (payload) => ({
  type: types.GET_ALL_MEDIA_OF_USER,
  payload
});

export const getEventsOfUser = (payload) => ({
  type: types.GET_EVENTS_OF_USER,
  payload
});

export const getMediaFromEvent = (payload) => ({
  type: types.GET_MEDIA_FROM_EVENT,
  payload
});

export const setMediaFromEvent = (payload) => ({
  type: types.SET_MEDIA_FROM_EVENT,
  payload
});

export const setMessage = (payload) => ({
  type: types.SET_MESSAGE_ACTION,
  payload
});

export const logout = (payload) => ({
  type: types.LOGOUT,
  payload
});
